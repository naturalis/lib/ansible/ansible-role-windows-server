# ansible-role-windows-server

Ansible role to configure Windows servers

## Example playbook

```yaml
- hosts: all
  gather_facts: true
  become: false
  tasks:
    - include_role:
        name: windows-base
```

## Required inputs


### Windows Updates
This role has one required input and that is the time and date a specific server has a maintenance window and the actual duration of the service window (in whole hours). In this Window a server can be updated and rebooted.

```yaml
windows_maintenance_window:
  day_of_week: monday
  hour: 19
  duration: 12
```

## Optional inputs

### Extra disk management

This setting allows the definition for initializing, partitioning and formatting extra disks. This will create a partition equal to the max disk size with and will format it with 
an NTFS file system. On disk resize, the disk should be increased manually.

To do this, the following variable can be set.

```yaml
windows_extra_disks:
  - disk_number: 1
    driveletter: F
    label: data
```
### Extra packages (Chocolatey)

This settings allows the installation of specific packages on top of the default packages (see vars/main/_windows_base_packages). See https://community.chocolatey.org/packages
to find out which packages are available

```yaml
windows_extra_packages:
  - googlechrome
  - notepadplusplus
```

### Windows features

This setting allows the installation of specific windows features

```yaml
windows_features:
  - Web-Server
  - Web-Common-Http
```
### Active Directory domain join

This setting is used to override any default dns servers set by DHCP for example

```yaml
windows_join_domain:
  dns_servers:
  - 10.0.0.1
  - 10.0.0.2
  domain: yourdomain.com
  netbios_domain: yourdomain
  join_account: join-account
  join_password: supersecretpassword
  ou_path: "OU=Servers,DC=yourdomain,DC=com"
```

### Backup

This settings allows you to install and configure a Veaam Backup agent. The default is false. If set to true, all other variables must be defined. The defined account needs at least read rights to the specified share. 


```yaml
windows_base_backup_enabled: true
windows_base_backup_veeam_agent_config: The content of the configuration file. This is generated a specific protection group in Veeam. This can be a base64 encoded string in vault for example.
windows_base_backup_veeam_installer_service_path: \\server\share\folder\VeeamInstallerSvc.msi
windows_base_install_repo_username: DOMAIN\username
windows_base_install_repo_password: password
```